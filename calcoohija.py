#!/usr/bin/python3
# -*- coding: utf-8 -*-
#LuisSánchez

import sys
import calcoo

class CalculadoraHija(calcoo.Calculadora):
    def mult(self, op1, op2):
        return op1 * op2

    def div(self, op1, op2):
        try:
            return op1 / op2
        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed")
    
if __name__ == "__main__":
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    
    h = CalculadoraHija()
    if sys.argv[2] == "suma":
        result = h.plus(operando1, operando2)
    elif sys.argv[2] == "resta":
        result = h.minus(operando1, operando2)
    elif sys.argv[2] == "multiplica":
        result = h.mult(operando1, operando2)
    elif sys.argv[2] == "divide":
        result = h.div(operando1, operando2)
    else:
        sys.exit('Operación sólo puede ser sumar, restar, multiplica, o divide')
    print(result)
