#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija

if __name__ == "__main__":
   
    calhija = calcoohija.CalculadoraHija()

    f = open(sys.argv[1],'r')

    fichero = f.readlines()
    
    for line in fichero:
        cadena = line.split(",")
        operacion = cadena[0]
        n1 = cadena[1]
        n2 = cadena[2:]
        for posicion in n2:
            if cadena [0] == "suma":
                n1 =  calhija.plus(float(n1), float(posicion))
                result = n1
            elif cadena [0] == "resta":
                n1 =  calhija.minus(float(n1), float(posicion))
                result = n1
            elif cadena [0] == "multiplica":
                n1 =  calhija.mult(float(n1), float(posicion))
                result = n1
            elif cadena [0] == "divide":
                n1 =  calhija.div(float(n1), float(posicion))
                result = n1
            else:
                print("Operación no válida")
    
        print(result)
        f.close
